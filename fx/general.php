<?php
function con(){
	$host 	= 	HOST;
	$db 	=	DB;
	$user 	= 	USER;
	$pass 	= 	PASS;

	mysqli_report(MYSQLI_REPORT_STRICT);

	try {
		$con = new mysqli($host,$user,$pass,$db);
		$con->query("set character_set_results='utf8'");
		return $con;
	}catch (Exception $e) {
		if (mysqli_connect_errno()) {
			die(" (╯°□°)╯︵ ┻━━━━ ".strrev("Failed to connect to MySQL: " . mysqli_connect_errno())." ━━━━┻") ;
		}
	}
}
function fetchalldata(String $query){
	$data = array();
	$result = con()->query($query);
	$data = mysqli_fetch_all($result,MYSQLI_ASSOC);
	con()->close();
	return $data;
}

function e(String $string){
	echo $string;
}

function getimgfromurl($domain,$type){
	if ($type=='fi') {
		$fetchUrl = "http://s2.googleusercontent.com/s2/favicons?domain_url=".urlencode($domain);
	}elseif ($type=='ss'){
		$apikey = "ab3129f665390bbd5aa0ab8b07bea3c79fe087990b7f";
		$width  = 560;
		$fetchUrl = "https://api.thumbnail.ws/api/".$apikey ."/thumbnail/get?url=".urlencode($domain)."&width=".$width;
	}

	return $fetchUrl;
}

function saveimg(String $domain,$type){
	$subdir = $type;

	$dir = STORAGE.IMG."/".$subdir."/";

	// To create the nested structure, the $recursive parameter 
	// to mkdir() must be specified.



	$filename = make_unique_id($domain);

	$fetchUrl = getimgfromurl($domain,$type);

	$image = file_get_contents($fetchUrl);
	$ext = 'webp';
	$img = new Imagick();
	$img->readImageBlob($image);
	$img->setImageFormat($ext);
	$img->setImageCompressionQuality(80);
	$img->setOption('webp:method', '6');
	if (!mkdir($dir, 0777, true)) {
	    $img->writeImage($dir.$filename.$ext);
	}else{
	    $img->writeImage($dir.$filename.$ext);
	}
	return $filename;
}

function base64_url_encode($input) {
	return strtr(base64_encode($input), '+/=', '-_.');
}

function base64_url_decode($input) {
	return base64_decode(strtr($input, '-_.', '+/='));
}

function make_unique_id($name,$salt = sha1(strtotime("now").rand(0,9))){
	$sha1 = sha1($name);
	$hash = sha1($salt.$sha1);
	return $hash;
}

function create_image($sql){
	$status = (con()->query($sql) === TRUE) ? true : false;
	con()->close();
	return $status;
}
